# LineageOS19DisableIntentFilter

Temporary workaround to disable Lineage OS v 19.1 's 'Intent Filter Verification stopped working' pop up

Copied from Github gist 
https://gist.github.com/lpnsk/220fcf3580f09cff16f3b8cdacb13e8b

---

## FIX : Intent filter verification service keeps stopping

Found this fix on reddit to keep this issue from happening on LineageOS 19.x with MicroG.
Root users :

Use the built-in terminal or Termux
```
  su
  pm disable-user --user 0 com.android.statementservice
```
Non-root users :

Hook the phone to a PC with android tools installed and USB debugging enabled, run this on your command line utility
```
  adb shell
  pm disable-user --user 0 com.android.statementservice
```